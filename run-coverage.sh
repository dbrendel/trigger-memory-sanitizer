#!/bin/bash

declare -a kernels
kernels+=(vmlinuz-5.14.0-164.gcov.el9.x86_64)
kernels+=(vmlinuz-5.14.0-164.gcov.el9.x86_64+debug)


declare -a test_functions
for fct in $(sed '/^ *FCT_LIST_ADD/!d;s/.*(\(.*\)).*/\1/' memsan_test.c); do
    test_functions+=($fct)
done
#test_functions+=(kmalloc_oob_right)

dir="coverage_results"

# My fork which has the 'enable-kasan' branch, which is 'main-automotive' + KASAN
# repo_kernel="https://gitlab.com/dbrendel/kernel-centos-stream-9.git"
repo_test="https://gitlab.com/dbrendel/trigger-memory-sanitizer.git"

ssh_args=""

gen_ssh_key() {
    local keyfile

    keyfile="$(mktemp ${dir:?}/test_kernels_ssh_key.XXXXXX)"
    chmod 600 $keyfile
    ssh-keygen -q -N "" -t ed25519 -f $keyfile >/dev/null <<< 'y'

    echo "$keyfile"
}

get_instance_ip() {
    local name="$1"
    local ip

    ip=$(testcloud instance list | grep -w $name | awk '{ print $2 }')
    echo $ip
}

get_instance_port() {
    local name="$1"
    local port

    port=$(testcloud instance list | grep -w $name | awk '{ print $3 }')
    echo $port
}

seconds_elapsed() {
    local start="$1"
    local elapsed
    local current

    current=$(date +%s)
    elapsed=$((current - start))

    echo $elapsed
}

wait_for_instance() {
    local timeout=600
    local ip
    local port
    local start

    test -n "$3" && timeout="$3"

    ip="$1"
    port="$2"

    start=$(date +%s)
    while test $(seconds_elapsed $start) -lt $timeout; do
        nc -z $ip $port && return 0
        sleep 1
    done

    return 1
}

cleanup() {
    test -d ${dir:?}/$(current_kernel) && rm -r ${dir:?}/$(current_kernel)
    exit
}

trap cleanup INT

current_kernel() {
    echo "vmlinuz-$(ssh $ssh_args uname -r)"
}

has_kernel() {
    local kernel="$1"

    if ssh $ssh_args test -f /boot/$kernel; then
        return 0
    else
        return 1
    fi
}

usage() {
    echo "$0 <user> <ip> <port>"
    echo " - OR - "
    echo "$0 <testcloud instance name>"
}

cmdline_to() {
    local instance_name
    local ipaddr
    local port
    local user
    local ssh_key
    local ssh_target

    if test $# -eq 2; then
        instance_name="$2"
        user=cloud-user
        ipaddr=$(get_instance_ip $instance_name)
        port=$(get_instance_port $instance_name)
    elif test $# -eq 4; then
        user="$2"
        ipaddr="$3"
        port="$4"
    else
        exit 1
    fi

    case $1 in
        "user")
            echo $user
            ;;
        "port")
            echo $port
            ;;
        "ipaddr")
            echo $ipaddr
            ;;
    esac
}

is_valid_repo() {
    local repo="$1"
    local dir
    local remote

    dir="$(basename --suffix=.git $repo)"

    ssh $ssh_args test -d $dir/.git
    if test $? -ne 0; then
        return 1
    fi

    remote="$(ssh -t $ssh_args "cd $dir/; git remote --verbose" \
             | grep "fetch" | awk '{ print $2 }')"

    if ! test "$remote" = "$repo"; then
        return 2
    fi

    return 0
}

create_repo() {
    local repo="$1"
    local dir

    dir="$(basename --suffix=.git $repo)"

    # Use shallow clone because otherwise we get that error:
    # fatal: fetch-pack: invalid index-pack output
    # Additionally we do not require more than the latest state
    ssh $ssh_args git clone --depth 1 $repo $dir
}

build_target() {
    local dir="$1"
    local target="$2"
    local njobs
    local privileged

    # I don't like it, this needs a better solution. Mayber another argument?
    if test -n "$(echo $target | grep install)"; then
        privileged="sudo"
    fi

    njobs=$(ssh $ssh_args nproc)
    ssh $ssh_args $privileged make --jobs $njobs -C $dir $target >/dev/null 2>&1
}

install_pkgs() {
    local pkgs="$1"
    local pkg_q

    for pkg in $pkgs; do
        if ! ssh $ssh_args rpm --quiet --query $pkg; then
            pkg_q+="$pkg "
        fi
    done

    if test -n "$pkg_q"; then
        ssh $ssh_args sudo dnf -y install $pkg_q
    fi
}

boot_kernel() {
    local kernel="$1"

    ssh $ssh_args sudo grubby --set-default=/boot/$kernel >/dev/null 2>&1
    if test $? -ne 0; then
        echo "Failed setting a new kernel!"
        exit 1
    fi

    ssh $ssh_args sudo reboot

    sleep 15
}

test_coverage() {
    local kernel="$1"

    if ! test "$kernel" = "$(current_kernel)"; then
        if ! has_kernel $kernel; then
            echo "$kernel not found! Skipping.."
            return
        fi
        echo "Booting kernel $kernel.."
        boot_kernel $kernel
    fi
    if ! wait_for_instance $ipaddr $port; then
        echo "Failed to detect listener on port $port at $ipaddr"
        exit 1
    fi

    mkdir --parents $dir/$kernel
    echo "Building module for kernel $kernel.."
    ssh $ssh_target rm $test_dir/\*.gc\*
    build_target $test_dir clean
    build_target $test_dir
    fetch_coverage_data $kernel --initial

    for function in "${test_functions[@]}"; do
        echo "$kernel: $function"
        ssh $ssh_args sudo insmod $test_dir/memsan_test.ko num_runs=1 tests=$function
        ssh $ssh_args sudo rmmod memsan_test
        touch $dir/$kernel/$function
    done

    fetch_coverage_data $kernel
}

fetch_coverage_data() {
    local kernel="$1"
    local initial="$2"
    local suffix
    local remote_test_dir

    if test -z "$initial"; then
        suffix="test"
    else
        suffix="base"
    fi

    remote_test_dir=$(ssh $ssh_args readlink -f $test_dir)

    echo "Copying coverage data.."
    if test -z "$initial"; then
        remote_uid=$(ssh $ssh_args id -u)
        ssh $ssh_args sudo install --owner=$remote_uid /sys/kernel/debug/gcov/$remote_test_dir/memsan_test.gcda \
                              $remote_test_dir/memsan_test.gcda
    fi

    ssh $ssh_args lcov $initial --capture --directory $remote_test_dir \
	               --output-file $remote_test_dir/memsan_test_$suffix.info

    if test -z "$initial"; then
        ssh $ssh_args lcov --add-tracefile $remote_test_dir/memsan_test_base.info \
                           --add-tracefile $remote_test_dir/memsan_test_test.info \
                           --output-file $remote_test_dir/memsan_test_total.info

        ssh $ssh_args cat $remote_test_dir/memsan_test.gcda > $dir/$kernel.gcda
        ssh $ssh_args cat $remote_test_dir/memsan_test.gcno > $dir/$kernel.gcno
        ssh $ssh_args cat $remote_test_dir/memsan_test_base.info > $dir/$kernel\_base.info
        ssh $ssh_args cat $remote_test_dir/memsan_test_test.info > $dir/$kernel\_test.info
        ssh $ssh_args cat $remote_test_dir/memsan_test_total.info > $dir/$kernel\_total.info

	ssh $ssh_args genhtml $remote_test_dir/memsan_test_total.info --output-directory $kernel\_out
	ssh $ssh_args tar cf - $kernel\_out | tar xfC - $dir/
    fi
}

main() {
    local ssh_opts
    local ssh_target
    local kernel
    local user
    local ipaddr
    local port
    local test_dir

    if ! test -d $dir; then
        mkdir $dir
    fi

    if   ! user=$(cmdline_to user "${@}") \
      || ! port=$(cmdline_to port "${@}") \
      || ! ipaddr=$(cmdline_to ipaddr "${@}"); then
      usage
      exit 1
    fi

    test_dir="$(basename --suffix=.git $repo_test)"

    ssh_target="-p $port $user@$ipaddr"

    test -z "$ssh_key" && ssh_key=$(gen_ssh_key)
    ssh_opts="-o StrictHostKeyChecking=no"
    ssh_opts+=" -o ControlMaster=auto"
    ssh_opts+=" -o ControlPath=$dir/%r@%h:%p"
    ssh_opts+=" -o ControlPersist=10m"
    ssh_opts+=" -i $ssh_key"

    ssh_args="$ssh_opts $ssh_target"

    if ! wait_for_instance $ipaddr $port; then
        echo "Failed to detect listener on port $port at $ipaddr"
        exit 1
    fi

    if ! ssh -q -o BatchMode=yes $ssh_args exit; then
        echo "Password-less login did not work. Copying pubkey.."
        ssh-copy-id $ssh_args >/dev/null 2>&1
    fi

    install_pkgs "gcc bc diffutils elfutils-libelf-devel bison flex openssl-devel
                  procps-ng util-linux-core git rpm-build python3-devel dwarves epel-release"
    install_pkgs "lcov"

    for repo in $repo_test; do
        if ! is_valid_repo $repo; then
            create_repo $repo || exit 1
        fi
    done

    for kernel in "${kernels[@]}"; do
        test_coverage $kernel
    done
}

main "${@}"

