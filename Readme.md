# Disclaimer

This is under development and can break your system. It does not handle all error cases and might ungracefully do stupid things. You have been warned!

# Trigger Memory Sanitizer

This project is based on the KASAN kunit test and has the purpose to assess memory access error detection rates for different types of access errors and not only for KASAN, but for KFENCE as well.

The main purpose is to get numbers on detection rates in order to formulate a functional safety argumentation around the ability to detect those errors.

## Requirements

- git
- bash
- target system (CentOS Stream 9 based virtual machine)

A number of different kernels is required and built by the script.

Due to the potentially destructive nature of the tests using a virtual machine as target system is strongly recommended. The test defaults to using the CentOS Stream 9 kernel and makes use of `rpm` and `dnf`, so using the same distribution is recommended.

The [testcloud](https://pagure.io/testcloud) tool can be used to quickly setup such a VM. It is shipped with the Fedora repos and EPEL. A VM setup is then as simple as that:

```bash
testcloud instance create --name memerr-test --vcpus $(nproc) --ram 8192 --disksize 70 centos-stream:9
```

Refer to `testcloud instance create --help` for more information.

The argument to `--name` is what is required for calling the test run script  next.

## Installation

```bash
git clone https://gitlab.com/dbrendel/trigger-memory-sanitizer.git
```

## Usage

```bash
./run-tests.sh memerr-test
```

or to connect to any system

```bash
./run-tests.sh <USER> <IP> <PORT>
```

This will collect kernel log output in hardcoded directory called `memsan_results`. The also provided script `parse-logs.sh` can transform that into csv by just calling

```bash
./parse-logs.sh
```

## Contributing

Any contribution is welcome!

