#!/bin/bash

dir=memsan_results

get_kfence_num_objects() {
    local dir="$1"

    echo $dir | sed 's/.*-\([0-9]\+\)-/\1/'
}

get_kfence_sample_intervals() {
    local dir="$1"
    local list_files

    for file in $(get_file_list $dir); do
        list_files+="$(basename $file | cut -d '_' -f 2) "
    done

    echo $list_files | tr ' ' '\n' | sort --numeric-sort --unique
}

get_kasan_mode() {
    local dir="$1"

    echo $dir | sed 's/.*kasan-\([a-z]\+\)-/\1/'
}

get_kfence_kernel_dir_list() {
    local root="$1"

    find $root -mindepth 1 -maxdepth 1 -type d | grep "kfence"
}

get_kasan_kernel_dir_list() {
    local root="$1"

    find $root -mindepth 1 -maxdepth 1 -type d | grep "kasan"
}

get_function_dir_list() {
    local dir="$1"

    find $dir -mindepth 1 -maxdepth 1 -type d | sort
}

get_file_list() {
    local dir="$1"

    find $dir -type f
}

get_file_list_filtered() {
    local dir="$1"
    local si="$2"

    get_file_list $dir | grep dmesg_$si\_
}

main() {
    local list_filtered

    local elem
    local avg
    local cnt
    local n
    local d
    local f
    local s

    printf "function,kasan_mode,num_events_detected\n"
    for k in $(get_kasan_kernel_dir_list $dir); do
        for d in $(get_function_dir_list $k); do
            f="$(basename $d)"
            n=0
            avg=0
            list_filtered=$(get_file_list $d)
            for elem in $list_filtered; do
                cnt=$(grep -c "BUG: KASAN" $elem)
                avg=$((avg+cnt))
                n=$((n+1))
            done
            avg=$(echo "$avg / $n" | bc -l)
            printf "%s,%s,%.2f\n" $f $(get_kasan_mode $k) $avg
        done
    done

    printf "function,num_objects,sample_interval,num_events_detected\n"
    for k in $(get_kfence_kernel_dir_list $dir); do
        for d in $(get_function_dir_list $k); do
            f="$(basename $d)"
            for s in $(get_kfence_sample_intervals $d); do
                n=0
                avg=0
                list_filtered=$(get_file_list_filtered $d $s)
                for elem in $list_filtered; do
                    cnt=$(grep -c "BUG: KFENCE" $elem)
                    avg=$((avg+cnt))
                    n=$((n+1))
                done
                avg=$(echo "$avg / $n" | bc -l)
                printf "%s,%d,%d,%.2f\n" $f $(get_kfence_num_objects $k) $s $avg
            done
        done
    done
}

main "${@}"

